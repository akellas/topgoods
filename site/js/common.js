$(document).ready(function() {
	'use sctrict';

	var hoverToggler = {
		init: function(options) {
			this.$el = $(options.el);
			this.$target = $(options.target);
			this.$delayIn = options.delayIn;
			this.$delayOut = options.delayOut;
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$el.hover(this._handlerIn.bind(this), this._handlerOut.bind(this));
		},
		_handlerIn: function(event) {
			this._showEl(event);
			this._showTarget();
		},
		_handlerOut: function(event) {
			this._hideEl(event);
			this._hideTarget();
		},
		_showTarget: function() {
			var self = this;
			setTimeout(function() {
				self.$target[0].classList.add("is-active");
			}, this.$delayIn);
			clearTimeout(this.timerTarget);
		},
		_hideTarget: function() {
			var self = this;
			this.timerTarget = setTimeout(function() {
				self.$target[0].classList.remove("is-active");
			}, this.$delayOut);
		},
		_showEl: function(event) {
			var selfEvent = event.currentTarget;
			setTimeout(function() {
				selfEvent.classList.add("is-active");
			}, this.$delayIn);
			clearTimeout(this.timerEl);
		},
		_hideEl: function(event) {
			var selfEvent = event.currentTarget;
			var timerEl = setTimeout(function() {
				selfEvent.classList.remove("is-active");
			}, this.$delayOut);
		}
	};
	hoverToggler.init({
		el: ".js-menu-dropdown",
		target: ".js-overlay",
		delayIn: 300,
		delayOut: 300
	});

	var promoSliderOpt = {
		adaptiveHeight: true,
		item: 1,
		slideMargin: 0,
		loop: true,
		pager: false,
		onBeforeSlide: function(el) {
			promoSliderNav.removeClass("is-active");
		},
		onAfterSlide: function(el) {
			var index = +el.getCurrentSlideCount();
			promoSliderNav.eq(index-1).addClass("is-active");
		}
	}
	var promoSlider = $(".js-promo-slider").lightSlider(promoSliderOpt); 
	var promoSliderNav = $(".js-promo-slider-nav");
	promoSliderNav.on("click", function() {
		var index = $(this).data("slide");
		promoSliderNav.removeClass("is-active");
		$(this).addClass("is-active");
		promoSlider.goToSlide(index);
	});

	var sliderOpt = {
		adaptiveHeight: true,
		item: 4,
		slideMove:4,
		slideMargin: 0,
		loop: false,
		pager: true
	}
	var slider = $(".js-slider").lightSlider(sliderOpt); 

	var sliderFullOpt = {
		adaptiveHeight: true,
		item: 1,
		slideMove:1,
		slideMargin: 0,
		loop: true,
		pager: false,
		controls: false,
		mode: "fade"
	}
	var sliderFull = $(".js-slider-full").lightSlider(sliderFullOpt);

	$(".js-slider-full-prev").on("click", function() {
		sliderFull.goToPrevSlide();
	});	
	$(".js-slider-full-next").on("click", function() {
		sliderFull.goToNextSlide();
	});	

	var productSliderOpt = {
		adaptiveHeight: true,
		item: 1,
		slideMove:1,
		slideMargin: 0,
		loop: true,
		pager: true,
		controls: false,
	}
	$(".js-product-slider").lightSlider(productSliderOpt);

	var classToggler = {
		init: function(options) {
			this.$el = $(".js-class-toggler");
			this._bindEvent();
		},
		_bindEvent: function() {
			this.$el.on("click", this._changeClass.bind(this));
		},
		_changeClass: function() {
			var elClass = event.currentTarget.dataset.class;
			var elTarget = event.currentTarget.dataset.target;
			document.querySelector(elTarget).classList.toggle(elClass);
			event.currentTarget.classList.toggle("is-active");
		}
	}
	classToggler.init(); 

	Waves.attach('.btn', ['waves-button', 'waves-light']);
	Waves.attach('.btn-nav button', ['waves-light']);
	Waves.init();


	$(".js-circle").each(function(i) {
		$(this).attr("id", "circle-"+i);
		Circles.create({
		  id:           $(this).attr("id"),
		  radius:       30,
		  value:        $(this).attr("data-val"),
		  maxValue:     $(this).attr("data-max"),
		  width:        2,
		  text:         function(value){return value},
		  colors:       ['#D6DADC', $(this).attr("data-color")],
		  duration:     400,
		  wrpClass:     'circles-wrp',
		  textClass:    'circles-text',
		  styleWrapper: true,
		  styleText:    true
		});
	});

	$('.js-gallery').lightGallery({
	    thumbnail:true,
	    animateThumb: false,
	    showThumbByDefault: false
	}); 

	

	// $(".js-ajax-tabs a").on("click", function() {
	// 	window.location.hash = this.hash;
	// 	$("#result").load( "ajax/product-tabs.html "+ this.hash);

	// 	$(".js-ajax-tabs li").removeClass("is-active");
	// 	$(this).parent().addClass("is-active");



	// });

	var ajaxTabs = {
		init: function(options) {
			this.$tabs = $(options.tabs);
			this.$content = $(options.content);
			this.$link = this.$tabs.find("a");
			this.$linkFirst = this.$tabs.find(".is-active a");
			this._firstLoad();
			this._onClick();
		},
		_onClick: function() {
			this.$link.on("click", this._loadContent.bind(this));
		},
		_loadContent: function(event) {
			window.location.hash = this.hash
			this.$content.load( "ajax/product-tabs.html "+ event.target.hash);
			this._changeStates();
		},
		_changeStates: function() {
			$(this.$tabs).find("li").removeClass("is-active");
			$(event.target).parent().addClass("is-active");
		},
		_firstLoad: function() {
			if(!window.location.hash) {
				var hash = $(this.$linkFirst).attr("href");
				this.$content.load( "ajax/product-tabs.html "+ hash);
			}
			else {
				var hash = window.location.hash;
				this.$content.load( "ajax/product-tabs.html "+ hash);
				$(this.$tabs).find("li").removeClass("is-active");
				$(this.$tabs).find('[href="'+hash+'"]').parent().addClass("is-active");
			}
		}
	}
	ajaxTabs.init({
		tabs: ".js-ajax-tabs",
		content: ".js-tabs-result"
	});

});